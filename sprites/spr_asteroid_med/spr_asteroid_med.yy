{
    "id": "11a88f6a-13ea-4a80-aa77-e93738f47c72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a551df-f7cf-4a22-9345-8d187ca94eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a88f6a-13ea-4a80-aa77-e93738f47c72",
            "compositeImage": {
                "id": "d4719aed-9d77-4a53-858d-4b8e4feda276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a551df-f7cf-4a22-9345-8d187ca94eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc9d5de-1608-4423-86e2-31ebec17c637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a551df-f7cf-4a22-9345-8d187ca94eda",
                    "LayerId": "23ea244b-1fa5-455e-bd92-8adb41813e66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "23ea244b-1fa5-455e-bd92-8adb41813e66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11a88f6a-13ea-4a80-aa77-e93738f47c72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}