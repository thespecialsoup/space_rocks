{
    "id": "029bac29-0789-40ec-be33-4aa79a33b272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6535baeb-a9b8-4db1-bb63-6cb891742e94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "029bac29-0789-40ec-be33-4aa79a33b272",
            "compositeImage": {
                "id": "f8ec5b4d-b74e-4b33-9749-9ba0ca96ddcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6535baeb-a9b8-4db1-bb63-6cb891742e94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a7f18e-cb48-4bb6-b9ac-ae195cb93822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6535baeb-a9b8-4db1-bb63-6cb891742e94",
                    "LayerId": "1749436b-5d19-4a6a-b9cc-c7889c2f0cde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "1749436b-5d19-4a6a-b9cc-c7889c2f0cde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "029bac29-0789-40ec-be33-4aa79a33b272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}