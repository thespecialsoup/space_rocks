{
    "id": "e87e54eb-f730-4b6e-a93b-4bf5eb3f7e29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 32,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3626ec1-da78-455b-90ce-e7bf8edd06a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e87e54eb-f730-4b6e-a93b-4bf5eb3f7e29",
            "compositeImage": {
                "id": "701dfe7c-87e1-45b1-8ac4-50df2d62d85e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3626ec1-da78-455b-90ce-e7bf8edd06a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99daa454-f7c3-4d53-b7af-ff9491c9e483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3626ec1-da78-455b-90ce-e7bf8edd06a4",
                    "LayerId": "c5f75b2b-928c-4cd1-aeb9-87349c154d99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c5f75b2b-928c-4cd1-aeb9-87349c154d99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e87e54eb-f730-4b6e-a93b-4bf5eb3f7e29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}