{
    "id": "784d8728-c1f0-41c7-8720-082c26f1de9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c44d0106-4d9b-44f8-ae39-de9396ec2766",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "784d8728-c1f0-41c7-8720-082c26f1de9d",
            "compositeImage": {
                "id": "db497e92-f8a2-4b83-b11d-aff8b8990ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44d0106-4d9b-44f8-ae39-de9396ec2766",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a90e2ce-217c-40ac-95b2-496e736231e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44d0106-4d9b-44f8-ae39-de9396ec2766",
                    "LayerId": "9d74686f-1b11-46eb-bfe2-f63e31f4bf12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9d74686f-1b11-46eb-bfe2-f63e31f4bf12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "784d8728-c1f0-41c7-8720-082c26f1de9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}