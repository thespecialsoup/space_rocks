{
    "id": "bf14278a-5323-4d03-a07b-50eae6ae93cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4fdb371-0e2f-4c17-bee4-290d90e1f318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf14278a-5323-4d03-a07b-50eae6ae93cf",
            "compositeImage": {
                "id": "07793c8c-84d9-44d9-b34f-5112167e79ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4fdb371-0e2f-4c17-bee4-290d90e1f318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f15f6f-43f3-4d22-b917-4f00ebcf8544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4fdb371-0e2f-4c17-bee4-290d90e1f318",
                    "LayerId": "385393ed-4935-4846-9bab-2d0fd8da3196"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "385393ed-4935-4846-9bab-2d0fd8da3196",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf14278a-5323-4d03-a07b-50eae6ae93cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}