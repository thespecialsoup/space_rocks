{
    "id": "749691fd-599b-43b5-acfc-85ff48d9c628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f20642c4-e2cd-47d8-b9e0-a969d1df54b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749691fd-599b-43b5-acfc-85ff48d9c628",
            "compositeImage": {
                "id": "58ded8cb-25c8-4a25-90b4-d786aceb03d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f20642c4-e2cd-47d8-b9e0-a969d1df54b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27984f9c-91c7-409d-8cd7-4c8561397adf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f20642c4-e2cd-47d8-b9e0-a969d1df54b2",
                    "LayerId": "e549649f-1ebe-4df6-a502-50507755a608"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e549649f-1ebe-4df6-a502-50507755a608",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749691fd-599b-43b5-acfc-85ff48d9c628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}