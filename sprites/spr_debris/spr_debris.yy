{
    "id": "8ef927ef-9da1-4bfc-9129-90cb0a840822",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3be08545-ed08-4f6f-bea3-a96608c63995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ef927ef-9da1-4bfc-9129-90cb0a840822",
            "compositeImage": {
                "id": "a2778d97-d8a5-4cd6-a708-88150c8a995f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be08545-ed08-4f6f-bea3-a96608c63995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0591532f-ae98-4c1d-932c-c28e2a82e593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be08545-ed08-4f6f-bea3-a96608c63995",
                    "LayerId": "9a9e06d3-40b6-43ca-b523-15e1938f0193"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "9a9e06d3-40b6-43ca-b523-15e1938f0193",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ef927ef-9da1-4bfc-9129-90cb0a840822",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}