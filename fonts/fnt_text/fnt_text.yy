{
    "id": "d8d3d70d-b866-41b8-aaed-afe4df3173a7",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e0d12496-b7bd-4b03-9cfa-d88591ee2735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3b8a3991-0e16-4b79-af98-d6bca145c7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 136,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "640ee599-9d67-4567-b5c2-65377bbd33a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 98,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "06a0db8f-09ef-426d-867f-ebebbe0c4a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a56d72b5-8c95-44ec-822d-3054d6a498f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 227,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f3712d83-a7d6-4aa7-ac34-958a0ad48cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 23
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "790b8d12-fdc9-4b93-aa06-8e01ed009d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "bec6c265-0bf5-49dc-9653-86625e76b3c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 131,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0375600a-0ed1-444e-8d7f-f7f8a6f73375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 112,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6952f3d7-2b51-4780-a076-6a34f6344b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e661ef04-7ffd-414b-8ed5-a45feb676601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 140,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b0ec5ca6-e471-4a3a-8fa6-1d85657dfcdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7d12d318-6d9e-4bd4-af74-27a81b83842d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 77,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "acadc23c-68e9-4486-a00e-ee2683f3427e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 70,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "089aec74-ee58-4fc6-9589-9507b8a34728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 126,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "03b0d53f-08e3-4b82-b4ba-a0daba7d8534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 147,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "73a4f331-ac13-4142-a83a-27cd39f98ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d47b337c-43b8-48fb-abc1-434db75704f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "65cde161-f42c-43d5-8626-cac9ede28b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 194,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bdda8ea1-b2e2-4458-99cf-5682b4d63289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f46d0de9-5a9d-4112-b2dc-2d574a29910c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "30e26d8b-b816-4dab-bf57-321ef5656cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1505e789-3388-4d62-a1f5-ec0557ad041f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 77,
                "y": 23
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7cd07b2b-a06d-463c-b170-4cee3b600cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "14554b07-baae-4068-b50d-e1e37ab24056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 167,
                "y": 23
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "14d7b603-2e53-4ea0-b279-bff406089cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5899abdf-397d-4f9a-be5e-3cc94384c663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 141,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dd377699-33bd-438a-a77e-81c275f78f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "49e96c28-75ca-42c6-99b4-227240822141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 158,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c87bd9b4-2724-4411-b2a6-5f6cc1fd0423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "25569809-4aaa-4546-92ce-9bdf1fa827a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 176,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "03cbfc86-369a-4592-9018-1bfdeb7972e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8c6053d2-99b9-4f45-b9a0-62ce346174a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8227c7a1-8545-4055-9324-0d9a54e3175a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ba977537-37bd-4f68-8409-a25b36bb4e36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 203,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dc12af1d-8781-4346-8438-e2a412a5f4fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 197,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a4669ecb-6232-4cb4-b006-3e798dccb06e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e5c2e81-368b-492b-9abd-e545e274d82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 221,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c2a4fb52-b6e8-47ee-838c-c9513fd4b32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0a85c409-890e-4afe-a4bc-0b3443e8e0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 217,
                "y": 23
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6ca07e53-5022-4dac-8831-4f7c2cf43458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 207,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "360c6e72-2171-4987-8803-44cad55e00dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6cfb79a5-0bbd-4a98-a407-a575d97b55a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3c03e5eb-d976-4ddb-acac-ec32ac5f78cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "559e9404-3b54-4589-8e05-c07d2a918687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 239,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "36053492-aca8-4788-bfd5-37f3c636c40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "22aa0c50-ed59-415b-8dee-7625f10bfdc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 157,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9302070c-5116-4fdd-a9a5-6af78c70f1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "88057bd8-df8e-49bb-b63c-c51b79b35fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fc1b3acf-9dc3-4058-8ff7-a1213ad46624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "66fa543b-1f57-4dce-84c8-4379749ce3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 137,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5221a992-bb55-4aa4-8a75-48b5143e015e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c69a888a-c317-41fe-992d-bcf62db8ee51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5dc90d20-ef4b-4f70-8048-010e231eeee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c5da4020-9f9a-424b-8ca8-e8778a159cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0bb86554-d04a-4d87-855d-a64e5847fd4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2df6811a-22a3-4c47-bc20-4c3849e374f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7199d388-87d7-41e9-a44c-08c9671ad319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7e2f2119-b93d-489e-9c99-fc231f7d1065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f8004018-9e5d-4e36-9c50-6d0910772f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 84,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3d01e25e-9664-4ddc-979f-7b831561a92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ac02c062-7811-4259-a43e-c019c8299780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 91,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "00cb41e2-26c3-42b6-b184-96f13cf4f273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 187,
                "y": 23
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "66883839-7526-4581-9982-c45dcda43b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4c097fb8-ff9a-4f13-aa7d-99eccd77d315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "16f84b63-42bb-43f1-adef-e791d7406cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "73858470-6a82-49bc-8c3b-cd33523fb0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "adf37006-25d2-479e-ad52-9d841a7dd770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5dcbe7be-8a51-4252-b89c-b48cb939787f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 97,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d61e57bc-c404-4539-8006-ca7ae534be0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bb5f747e-4fae-43e2-a111-d0c31e2eb7cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "45ac65ff-2c4a-43fc-8a46-0e7711d79ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4d37b5ea-830e-4864-9ed5-629b7ea6e8f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bbb91922-d40e-4cd5-98b2-1070efe31fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 44
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bf89692a-1f87-4c2e-830e-48e7f51e98ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 46,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "777ccc5b-aa07-4f02-994a-e96a7a5eff4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0f4e1338-1aaf-4ce8-bf10-9e9d73f520cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "eb99216e-5112-4e6e-9590-10da11521ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b8b606b5-2efc-42f1-a400-010a62ee7964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 104,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "611792fb-e6c9-4b19-9ea7-b925a3819075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5278a0e9-c3c9-44f4-816e-28f8fc5018bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c4cdd6fd-fe4e-4541-b05d-02d46ffde69f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c95a739e-0baa-4a20-8675-195bab586c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d04146d7-6b7f-4887-89eb-a166d79116c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f8aa43d4-ca9b-4edc-9bc6-b1d121ae6fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 237,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "27a2c92b-0a9f-4d48-9588-1b212b5e94d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "25bf045b-1a34-446a-9c9f-7d8a1e413710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0f952051-6771-4011-adc0-1e6514265556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "dd5df16e-7f6c-4d91-896f-66ac472735f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8d41f1e6-1bc7-4c87-9de1-049b78576c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7676cd34-035c-4623-aee8-4c32a7489b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a8d9281c-0584-496a-af6b-1df0b41198a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bc84c790-9c85-47eb-a8a9-13a22726547f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b3b5fddf-fc0f-4252-92fe-bf245d680552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "59687b95-b10c-401f-875b-13e494715530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}