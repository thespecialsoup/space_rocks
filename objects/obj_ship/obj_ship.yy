{
    "id": "49170f65-0d26-44b6-b07a-eb54d9b2c241",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "0c9654f8-285e-40a5-a19b-e7a8d2ff862d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49170f65-0d26-44b6-b07a-eb54d9b2c241"
        },
        {
            "id": "04d1f616-cb01-4956-9db7-75cec2b61e5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3f933533-1ab3-4c29-a940-7a8fe1a84aa2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "49170f65-0d26-44b6-b07a-eb54d9b2c241"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e87e54eb-f730-4b6e-a93b-4bf5eb3f7e29",
    "visible": true
}