{
    "id": "3f933533-1ab3-4c29-a940-7a8fe1a84aa2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "4d5462b0-2ee4-4e2e-b6a6-f728804de87c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f933533-1ab3-4c29-a940-7a8fe1a84aa2"
        },
        {
            "id": "8fd6e19c-9451-45a4-afac-15ebe629b2d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f933533-1ab3-4c29-a940-7a8fe1a84aa2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "749691fd-599b-43b5-acfc-85ff48d9c628",
    "visible": true
}