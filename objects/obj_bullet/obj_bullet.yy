{
    "id": "82f6f526-0fa5-4ba7-9b6e-f3b6ca27f1bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "fa78eaeb-677c-4211-87e2-0f33221d8ba0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82f6f526-0fa5-4ba7-9b6e-f3b6ca27f1bf"
        },
        {
            "id": "876a9bdd-28ef-4762-b56c-92915671fe34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3f933533-1ab3-4c29-a940-7a8fe1a84aa2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "82f6f526-0fa5-4ba7-9b6e-f3b6ca27f1bf"
        },
        {
            "id": "d475005b-a07c-4c2a-83b0-1703d5213331",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "82f6f526-0fa5-4ba7-9b6e-f3b6ca27f1bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "029bac29-0789-40ec-be33-4aa79a33b272",
    "visible": true
}